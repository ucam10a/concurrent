package test;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class DelayedElement implements Delayed {

    private long fireTime;
    
    private String operation;
    
    public DelayedElement(String operation){
        this.operation = operation;
    }
    
    public DelayedElement(String operation, int delayMillis){
        this.operation = operation;
        this.fireTime = System.currentTimeMillis() + delayMillis;
    }
    
    @Override
    public int compareTo(Delayed o) {
        if (o instanceof DelayedElement){
            DelayedElement obj = (DelayedElement) o;
            if (this.operation.compareTo(obj.operation) > 0){
                return 1;
            } else if (this.operation.compareTo(obj.operation) < 0){
                return -1;
            }
        }
        return 0;
    }

    @Override
    public long getDelay(TimeUnit unit) {
        long diff = fireTime - System.currentTimeMillis();
        return unit.convert(diff, TimeUnit.MILLISECONDS);
    }
    
    public void doAction() {
        System.out.println(operation);
    }
    
}
