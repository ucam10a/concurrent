package test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Test {

    private static Token getToken(CopyOnWriteArrayList<Token> tokens) {
        for (Token token : tokens) {
            if (token.isFree() == true) {
                token.setFree(false);
                return token;
            }
        }
        return null;
    }
    
    public static void main(String args[]) throws Exception {
        
        int threadSize = 10;
        final CopyOnWriteArrayList<Token> tokens = new CopyOnWriteArrayList<Token>();
        for (int i = 0; i < threadSize; i++) {
            Token t = new Token("" + i);
            tokens.add(t);
        }
        CopyOnWriteArrayList<Runnable> taskList = new CopyOnWriteArrayList<Runnable>();
        List<Integer> idxList = new ArrayList<Integer>();
        for (int i = 0; i < 50; i++) {
            idxList.add(i);
        }
        for (final Integer i : idxList) {
            Runnable task = new Runnable() {
                @Override
                public void run() {
                    try {
                        Random ran = new Random();
                        Token token = getToken(tokens);
                        System.out.println("start " + i + ", use token " + token.getValue());
                        Thread.sleep(ran.nextInt(10000));
                        token.setFree(true);
                        System.out.println(i + " done, free token " + token.getValue());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            taskList.add(task);
        }
        long start = System.currentTimeMillis();
        System.out.println("start task ...");
        ConcurrentTerminationTool.executeAndWaitTermination(threadSize, taskList, 1000, 100);
        System.out.println("lapse: " + (System.currentTimeMillis() - start));
        
        System.out.println("all done!");
    }

}
