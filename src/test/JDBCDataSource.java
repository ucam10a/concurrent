package test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCDataSource {

    private Connection conn;

    public Connection getConnection() {
        String driver = "org.hsqldb.jdbcDriver";
        String user = "sa";
        String password = "";
        String url = "jdbc:hsqldb:hsql://localhost/test";
        return getConnection(driver, user, password, url);    
    }
    
    public Connection getConnection(String driver, String user, String password, String url) {
        try {
            try {
                Class.forName(driver);
            } catch (Exception e) {
                System.out.println("ERROR: failed to load JDBC driver.");
                e.printStackTrace();
                return null;
            }
            conn = DriverManager.getConnection(url, user, password);

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        return conn;
    }

}