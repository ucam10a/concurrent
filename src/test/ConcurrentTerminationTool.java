package test;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConcurrentTerminationTool {

    private static final Logger logger = Logger.getLogger(ConcurrentTerminationTool.class.getName());
    
    private static void awaitTerminationAfterShutdown(ExecutorService threadPool, int waitSeconds) {
        threadPool.shutdown();
        try {
            if (!threadPool.awaitTermination(waitSeconds, TimeUnit.SECONDS)) {
                threadPool.shutdownNow();
            }
        } catch (InterruptedException ex) {
            logger.log(Level.WARNING, ex.toString(), ex);
            threadPool.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }
    
    public static void executeAndWaitTermination(ExecutorService threadPool, CopyOnWriteArrayList<Runnable> taskList, int waitSeconds, int delay) {
        for (Runnable task : taskList) {
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                logger.log(Level.WARNING, e.toString(), e);
            }
            threadPool.execute(task);    
        }
        awaitTerminationAfterShutdown(threadPool, waitSeconds);
    }
    
    public static void executeAndWaitTermination(int poolSize, CopyOnWriteArrayList<Runnable> taskList, int waitSeconds) {
        executeAndWaitTermination(poolSize, taskList, waitSeconds, 500);
    }
    
    public static void executeAndWaitTermination(int poolSize, CopyOnWriteArrayList<Runnable> taskList, int waitSeconds, int delay) {
        ExecutorService threadPool = Executors.newScheduledThreadPool(poolSize);
        for (Runnable task : taskList) {
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                logger.log(Level.WARNING, e.toString(), e);
            }
            threadPool.execute(task);    
        }
        awaitTerminationAfterShutdown(threadPool, waitSeconds);
    }
    
    public static <T> T awaitFuture(Future<T> future, int waitSeconds) throws InterruptedException, ExecutionException {
        long start = System.currentTimeMillis();
        while (true) {
            if (future.isDone()) {
                return future.get();
            }
            if ((System.currentTimeMillis() - start) > (waitSeconds * 1000)) {
                break;
            }
            Thread.sleep(100);
        }
        if (future.isDone()) {
            return future.get();
        }
        return null;
    }
    
}
