package test;

public class Token {

    private boolean free = true;
    
    private String value;
    
    public Token (String value) {
        this.value = value;
    }

    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
}
