package test;

import java.util.concurrent.DelayQueue;

public class DelayQueueExample {

    public static void main(String[] args) throws InterruptedException {
        
        DelayQueue<DelayedElement> queue = new DelayQueue<DelayedElement>();
        DelayedElement action1 = new DelayedElement("delay 2000", 2000);
        DelayedElement action2 = new DelayedElement("delay 1000", 1000);
        queue.put(action1);
        queue.put(action2);
        
        queue.take().doAction();
        queue.take().doAction();
        
    }
    
}
