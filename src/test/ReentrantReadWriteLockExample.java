package test;

import java.util.concurrent.locks.ReentrantReadWriteLock;

import test.lock.Read;
import test.lock.WriteA;
import test.lock.WriteB;

public class ReentrantReadWriteLockExample {

    private static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock(true);

    public static String message = "a";

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new Read(lock));
        Thread t2 = new Thread(new WriteA(lock));
        Thread t3 = new Thread(new WriteB(lock));
        t2.start();
        t3.start();
        t1.start();
    }

}