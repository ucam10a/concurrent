package test.lock;

import java.util.concurrent.locks.ReentrantReadWriteLock;

import test.ReentrantReadWriteLockExample;

public class WriteA implements Runnable {
    
    private ReentrantReadWriteLock lock;
    
    public WriteA(ReentrantReadWriteLock lock) {
        this.lock = lock;
    }
    
    public void run() {
        for (int i = 0; i <= 10; i++) {
            try {
                System.out.println("WriteA get lock");
                lock.writeLock().lock();
                ReentrantReadWriteLockExample.message = ReentrantReadWriteLockExample.message.concat("a");
            } finally {
                lock.writeLock().unlock();
            }
        }
    }
}
