package test.lock;

import java.util.concurrent.locks.ReentrantReadWriteLock;

import test.ReentrantReadWriteLockExample;

public class Read implements Runnable {
    
    private ReentrantReadWriteLock lock;
    
    public Read(ReentrantReadWriteLock lock) {
        this.lock = lock;
    }
    
    public void run() {
        for (int i = 0; i <= 10; i++) {
            if (lock.isWriteLocked()) {
                System.out.println("I'll take the lock from Write");
            }
            System.out.println("Read get lock");
            lock.readLock().lock();
            System.out.println("ReadThread " + Thread.currentThread().getId() + " ---> Message is " + ReentrantReadWriteLockExample.message);
            lock.readLock().unlock();
        }
    }
}
