package test.lock;

import java.util.concurrent.locks.ReentrantReadWriteLock;

import test.ReentrantReadWriteLockExample;

public class WriteB implements Runnable {
    
    private ReentrantReadWriteLock lock;
    
    public WriteB(ReentrantReadWriteLock lock) {
        this.lock = lock;
    }
    
    public void run() {
        for (int i = 0; i <= 10; i++) {
            try {
                System.out.println("WriteB get lock");
                lock.writeLock().lock();
                ReentrantReadWriteLockExample.message = ReentrantReadWriteLockExample.message.concat("b");
            } finally {
                lock.writeLock().unlock();
            }
        }
    }
}
